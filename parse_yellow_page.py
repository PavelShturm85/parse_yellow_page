import threading
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

settings = {
    "chrome_driver": "./chromedriver",
    "search_word": "Бетон",
    "city_select": "city_select",
    "city_list": "ul.ui_dropdown_scrollable.ui_standart_select_list.ui_dropdown_body li a[href]",
    "company_name": ".company_name a",
    "company_url": ".company_url a",
    "company_address": ".unmargined.company_address",
    "btn_phone_display": ".phone-display",
    "company_phone": ".js-utm-ignore.company_phone",
    "btn_next_page": ".ui_next_page",
    "btn_next_page_disable": ".ui_next_page.ui_unactive",
    "msg_error": "#error-header",
    "msg_not_data": ".white_wrapper.found_nothing h2",
}


def count_time(f):
    def func(*arg, **kwarg):
        start_time = time.time()
        result = f(*arg, **kwarg)
        end_time = time.time()
        print(end_time - start_time)
        return result
    return func


class ParseYellowPages():
    def __init__(self, search_word=settings["search_word"]):
        self.template_url = "https://{city}.yp.ru/search/text/{search_word}/"
        self.search_word = search_word
        mongo = MongoClient('127.0.0.1', 27017, connect=False)
        db = mongo.companies_db
        self.company = db.yellow_pages_companies
        self.lock = threading.Lock()
        chrome = webdriver.Chrome()
        chrome.implicitly_wait(3)
        chrome.set_window_size(1920, 1080)
        chrome.maximize_window()
        self.chrome = chrome

    def __move_and_click(self, element, pause_time):
        ActionChains(self.chrome).move_to_element(
            element).pause(pause_time).click().perform()

    def __get_companies_list(self):
        btn_phone_display = WebDriverWait(self.chrome, 2).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, settings["btn_phone_display"])))
        for btn in btn_phone_display:
            try:
                self.__move_and_click(btn, 1)
            except:
                pass
        company_name = [name.text for name in self.chrome.find_elements_by_css_selector(
            settings["company_name"])]
        company_url = [url.text for url in self.chrome.find_elements_by_css_selector(
            settings["company_url"])]
        company_address = [address.text for address in self.chrome.find_elements_by_css_selector(
            settings["company_address"])]
        company_phone = [phone.text for phone in self.chrome.find_elements_by_css_selector(
            settings["company_phone"])]
        company_list = zip(company_name, company_url,
                           company_phone, company_address)
        yield from company_list

    def __create_company_dict_list(self):
        for company in self.__get_companies_list():
            yield {
                "title": company[0] or '',
                "url": company[1] or '',
                "phone": company[2] or '',
                "address": company[3] or '',
            }

    def __save_company(self, company):
        with self.lock:
            self.company.save(company)
            print("save: {}".format(company))

    def __is_in_base(self, company_detail):
        with self.lock:
            is_in_base = self.company.find_one(company_detail)
            if is_in_base:
                print("организация уже в базе")
        return bool(is_in_base)

    def __push_companies_to_save(self):
        company_dict_list = self.__create_company_dict_list()
        for company in company_dict_list:
            if not self.__is_in_base(company) and not "сеть адресов" in company.get("address", ""):
                self.__save_company(company)

    def _get_companies_from_city(self, num, city_list):
        for city in city_list:

            with self.lock:
                checked_city = self.company.find({"checked_point_{}".format(num): True})[0] or {}
            if not city in checked_city.get("checked_city", []):
                url = self.template_url.format(search_word=self.search_word, city=city)
                self.chrome.get(url)
                query = {"checked_point_{}".format(num): True}
                data = {"checked_city": city}
                with self.lock:
                    self.company.update(query, {'$push': data}, upsert=True)
                try:
                    msg_error = self.chrome.find_element_by_css_selector(
                        settings["msg_error"])
                    continue
                except:
                    pass
                try:
                    msg_not_data = self.chrome.find_element_by_css_selector(
                        settings["msg_not_data"])
                    continue
                except:
                    pass
                try:
                    self.__push_companies_to_save()
                except:
                    continue
                
                try:
                    btn_next_page = self.chrome.find_element_by_css_selector(settings["btn_next_page"])
                    while btn_next_page:
                       
                        self.__move_and_click(btn_next_page, 1)
                        try:
                            self.__push_companies_to_save()
                        except:
                            continue
                        
                        try:
                            btn_next_page_disable = self.chrome.find_element_by_css_selector(settings["btn_next_page_disable"])
                            break
                        except:
                            btn_next_page = self.chrome.find_element_by_css_selector(settings["btn_next_page"])
                except:
                    pass



class CreateTargetsAndParams():

    def __init__(self):
        self.url = "https://www.yp.ru/"
        chrome = webdriver.Chrome()
        chrome.implicitly_wait(3)
        chrome.set_window_size(1920, 1080)
        chrome.maximize_window()
        self.chrome = chrome

    def __select_number_threads(self):
        import multiprocessing
        cpu_count = int(multiprocessing.cpu_count())
        if cpu_count > 4:
            threads = cpu_count - 2
        elif 1 < cpu_count <= 4:
            threads = cpu_count - 1
        else:
            threads = cpu_count
        return threads

    def __get_sity_list(self):
        self.chrome.get(self.url)
        self.chrome.find_element_by_id(settings["city_select"]).click()
        cities = self.chrome.find_elements_by_css_selector(
            settings["city_list"])
        return [city.get_attribute("ui_val") for city in cities]

    def __chunkify(self, lst, n):
        return [lst[i::n] for i in range(n)]

    def _get_targets_and_params(self):
        step = self.__select_number_threads()
        sity_list = self.__get_sity_list()
        chunkify = self.__chunkify(sity_list, step)
        params = []
        target = []
        for num, param in enumerate(chunkify):
            parse_yp = ParseYellowPages()
            target.append(parse_yp._get_companies_from_city)
            params.append([num, param])

        return zip(target, params)


def main():
    targets_and_params = CreateTargetsAndParams()
  
    for target, param in targets_and_params._get_targets_and_params():
        print(param)
        my_thread = threading.Thread(target=target, args=(*param,))
        my_thread.start()

if __name__ == "__main__":
    main()
